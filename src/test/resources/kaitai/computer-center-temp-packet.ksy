meta:
  id: computer_center_temp_packet
  endian: be
  title: IT-FA-HCT LoRa Computer Center Temp packet (a Cayenne LPP packet)
  doc-ref: https://docs.mydevices.com/docs/lorawan/cayenne-lpp

seq:
  - id: packet
    size-eos: true
    type:
      switch-on: _io.size
      cases:
        15: cayenne_packet
        _: unsupported_frame

types:
  cayenne_packet:
    seq:
      - id: records
        type: cayenne_record
        repeat: eos

  cayenne_record:
    seq:
      - id: channel_hidden
        type: u1
      - id: sensor_type_hidden
        type: u1
      - id: sensor_data
        type:
          switch-on: channel_hidden
          cases:
            1: temperature_sensor
            2: humidity_sensor
            3: dew_point_sensor
            4: analog_input_sensor

  temperature_sensor:
    seq:
      - id: temperature_hidden
        type: s2
    instances:
      temperature:
        value: temperature_hidden / 10.0

  dew_point_sensor:
    seq:
      - id: dew_point_hidden
        type: s2
    instances:
      dew_point:
        value: dew_point_hidden / 10.0

  humidity_sensor:
    seq:
      - id: relative_humidity_hidden
        type: u1
    instances:
      relative_humidity:
        value: relative_humidity_hidden / 2.0

  analog_input_sensor:
    seq:
      - id: differential_pressure_hidden
        type: s2
    instances:
      differential_pressure:
        value: differential_pressure_hidden / 100.0

  unsupported_frame:
    seq:
      - id: unsupported_data
        size-eos: true
        doc: Unsupported packet data
