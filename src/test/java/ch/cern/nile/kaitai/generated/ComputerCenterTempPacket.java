// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

package ch.cern.nile.kaitai.generated;

import java.io.IOException;
import java.util.ArrayList;

import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStream;
import io.kaitai.struct.KaitaiStruct;


/**
 * @see <a href="https://docs.mydevices.com/docs/lorawan/cayenne-lpp">Source</a>
 */
public class ComputerCenterTempPacket extends KaitaiStruct {
    public static ComputerCenterTempPacket fromFile(String fileName) throws IOException {
        return new ComputerCenterTempPacket(new ByteBufferKaitaiStream(fileName));
    }

    public ComputerCenterTempPacket(KaitaiStream _io) {
        this(_io, null, null);
    }

    public ComputerCenterTempPacket(KaitaiStream _io, KaitaiStruct _parent) {
        this(_io, _parent, null);
    }

    public ComputerCenterTempPacket(KaitaiStream _io, KaitaiStruct _parent, ComputerCenterTempPacket _root) {
        super(_io);
        this._parent = _parent;
        this._root = _root == null ? this : _root;
        _read();
    }
    private void _read() {
        switch ((int) _io().size()) {
        case 15: {
            this._raw_packet = this._io.readBytesFull();
            KaitaiStream _io__raw_packet = new ByteBufferKaitaiStream(_raw_packet);
            this.packet = new CayennePacket(_io__raw_packet, this, _root);
            break;
        }
        default: {
            this._raw_packet = this._io.readBytesFull();
            KaitaiStream _io__raw_packet = new ByteBufferKaitaiStream(_raw_packet);
            this.packet = new UnsupportedFrame(_io__raw_packet, this, _root);
            break;
        }
        }
    }
    public static class UnsupportedFrame extends KaitaiStruct {
        public static UnsupportedFrame fromFile(String fileName) throws IOException {
            return new UnsupportedFrame(new ByteBufferKaitaiStream(fileName));
        }

        public UnsupportedFrame(KaitaiStream _io) {
            this(_io, null, null);
        }

        public UnsupportedFrame(KaitaiStream _io, ComputerCenterTempPacket _parent) {
            this(_io, _parent, null);
        }

        public UnsupportedFrame(KaitaiStream _io, ComputerCenterTempPacket _parent, ComputerCenterTempPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.unsupportedData = this._io.readBytesFull();
        }
        private byte[] unsupportedData;
        private ComputerCenterTempPacket _root;
        private ComputerCenterTempPacket _parent;

        /**
         * Unsupported packet data
         */
        public byte[] unsupportedData() { return unsupportedData; }
        public ComputerCenterTempPacket _root() { return _root; }
        public ComputerCenterTempPacket _parent() { return _parent; }
    }
    public static class CayenneRecord extends KaitaiStruct {
        public static CayenneRecord fromFile(String fileName) throws IOException {
            return new CayenneRecord(new ByteBufferKaitaiStream(fileName));
        }

        public CayenneRecord(KaitaiStream _io) {
            this(_io, null, null);
        }

        public CayenneRecord(KaitaiStream _io, CayennePacket _parent) {
            this(_io, _parent, null);
        }

        public CayenneRecord(KaitaiStream _io, CayennePacket _parent, ComputerCenterTempPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.channelHidden = this._io.readU1();
            this.sensorTypeHidden = this._io.readU1();
            switch (channelHidden()) {
            case 1: {
                this.sensorData = new TemperatureSensor(this._io, this, _root);
                break;
            }
            case 2: {
                this.sensorData = new HumiditySensor(this._io, this, _root);
                break;
            }
            case 3: {
                this.sensorData = new DewPointSensor(this._io, this, _root);
                break;
            }
            case 4: {
                this.sensorData = new AnalogInputSensor(this._io, this, _root);
                break;
            }
            }
        }
        private int channelHidden;
        private int sensorTypeHidden;
        private KaitaiStruct sensorData;
        private ComputerCenterTempPacket _root;
        private CayennePacket _parent;
        public int channelHidden() { return channelHidden; }
        public int sensorTypeHidden() { return sensorTypeHidden; }
        public KaitaiStruct sensorData() { return sensorData; }
        public ComputerCenterTempPacket _root() { return _root; }
        public CayennePacket _parent() { return _parent; }
    }
    public static class HumiditySensor extends KaitaiStruct {
        public static HumiditySensor fromFile(String fileName) throws IOException {
            return new HumiditySensor(new ByteBufferKaitaiStream(fileName));
        }

        public HumiditySensor(KaitaiStream _io) {
            this(_io, null, null);
        }

        public HumiditySensor(KaitaiStream _io, CayenneRecord _parent) {
            this(_io, _parent, null);
        }

        public HumiditySensor(KaitaiStream _io, CayenneRecord _parent, ComputerCenterTempPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.relativeHumidityHidden = this._io.readU1();
        }
        private Double relativeHumidity;
        public Double relativeHumidity() {
            if (this.relativeHumidity != null)
                return this.relativeHumidity;
            double _tmp = (double) ((relativeHumidityHidden() / 2.0));
            this.relativeHumidity = _tmp;
            return this.relativeHumidity;
        }
        private int relativeHumidityHidden;
        private ComputerCenterTempPacket _root;
        private CayenneRecord _parent;
        public int relativeHumidityHidden() { return relativeHumidityHidden; }
        public ComputerCenterTempPacket _root() { return _root; }
        public CayenneRecord _parent() { return _parent; }
    }
    public static class DewPointSensor extends KaitaiStruct {
        public static DewPointSensor fromFile(String fileName) throws IOException {
            return new DewPointSensor(new ByteBufferKaitaiStream(fileName));
        }

        public DewPointSensor(KaitaiStream _io) {
            this(_io, null, null);
        }

        public DewPointSensor(KaitaiStream _io, CayenneRecord _parent) {
            this(_io, _parent, null);
        }

        public DewPointSensor(KaitaiStream _io, CayenneRecord _parent, ComputerCenterTempPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.dewPointHidden = this._io.readS2be();
        }
        private Double dewPoint;
        public Double dewPoint() {
            if (this.dewPoint != null)
                return this.dewPoint;
            double _tmp = (double) ((dewPointHidden() / 10.0));
            this.dewPoint = _tmp;
            return this.dewPoint;
        }
        private short dewPointHidden;
        private ComputerCenterTempPacket _root;
        private CayenneRecord _parent;
        public short dewPointHidden() { return dewPointHidden; }
        public ComputerCenterTempPacket _root() { return _root; }
        public CayenneRecord _parent() { return _parent; }
    }
    public static class TemperatureSensor extends KaitaiStruct {
        public static TemperatureSensor fromFile(String fileName) throws IOException {
            return new TemperatureSensor(new ByteBufferKaitaiStream(fileName));
        }

        public TemperatureSensor(KaitaiStream _io) {
            this(_io, null, null);
        }

        public TemperatureSensor(KaitaiStream _io, CayenneRecord _parent) {
            this(_io, _parent, null);
        }

        public TemperatureSensor(KaitaiStream _io, CayenneRecord _parent, ComputerCenterTempPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.temperatureHidden = this._io.readS2be();
        }
        private Double temperature;
        public Double temperature() {
            if (this.temperature != null)
                return this.temperature;
            double _tmp = (double) ((temperatureHidden() / 10.0));
            this.temperature = _tmp;
            return this.temperature;
        }
        private short temperatureHidden;
        private ComputerCenterTempPacket _root;
        private CayenneRecord _parent;
        public short temperatureHidden() { return temperatureHidden; }
        public ComputerCenterTempPacket _root() { return _root; }
        public CayenneRecord _parent() { return _parent; }
    }
    public static class CayennePacket extends KaitaiStruct {
        public static CayennePacket fromFile(String fileName) throws IOException {
            return new CayennePacket(new ByteBufferKaitaiStream(fileName));
        }

        public CayennePacket(KaitaiStream _io) {
            this(_io, null, null);
        }

        public CayennePacket(KaitaiStream _io, ComputerCenterTempPacket _parent) {
            this(_io, _parent, null);
        }

        public CayennePacket(KaitaiStream _io, ComputerCenterTempPacket _parent, ComputerCenterTempPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.records = new ArrayList<CayenneRecord>();
            {
                int i = 0;
                while (!this._io.isEof()) {
                    this.records.add(new CayenneRecord(this._io, this, _root));
                    i++;
                }
            }
        }
        private ArrayList<CayenneRecord> records;
        private ComputerCenterTempPacket _root;
        private ComputerCenterTempPacket _parent;
        public ArrayList<CayenneRecord> records() { return records; }
        public ComputerCenterTempPacket _root() { return _root; }
        public ComputerCenterTempPacket _parent() { return _parent; }
    }
    public static class AnalogInputSensor extends KaitaiStruct {
        public static AnalogInputSensor fromFile(String fileName) throws IOException {
            return new AnalogInputSensor(new ByteBufferKaitaiStream(fileName));
        }

        public AnalogInputSensor(KaitaiStream _io) {
            this(_io, null, null);
        }

        public AnalogInputSensor(KaitaiStream _io, CayenneRecord _parent) {
            this(_io, _parent, null);
        }

        public AnalogInputSensor(KaitaiStream _io, CayenneRecord _parent, ComputerCenterTempPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.differentialPressureHidden = this._io.readS2be();
        }
        private Double differentialPressure;
        public Double differentialPressure() {
            if (this.differentialPressure != null)
                return this.differentialPressure;
            double _tmp = (double) ((differentialPressureHidden() / 100.0));
            this.differentialPressure = _tmp;
            return this.differentialPressure;
        }
        private short differentialPressureHidden;
        private ComputerCenterTempPacket _root;
        private CayenneRecord _parent;
        public short differentialPressureHidden() { return differentialPressureHidden; }
        public ComputerCenterTempPacket _root() { return _root; }
        public CayenneRecord _parent() { return _parent; }
    }
    private KaitaiStruct packet;
    private ComputerCenterTempPacket _root;
    private KaitaiStruct _parent;
    private byte[] _raw_packet;
    public KaitaiStruct packet() { return packet; }
    public ComputerCenterTempPacket _root() { return _root; }
    public KaitaiStruct _parent() { return _parent; }
    public byte[] _raw_packet() { return _raw_packet; }
}
