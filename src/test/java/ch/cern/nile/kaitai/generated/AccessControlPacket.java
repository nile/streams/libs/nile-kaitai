package ch.cern.nile.kaitai.generated;// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild


import java.io.IOException;

import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStream;
import io.kaitai.struct.KaitaiStruct;


/**
 * @see <a href="https://adeunis.notion.site/Technical-Reference-Manual-DRY-CONTACTS-LoRaWAN-Sigfox-c4540f166e484a269b4fcb79c090a587">Source</a>
 */
public class AccessControlPacket extends KaitaiStruct {
    public static AccessControlPacket fromFile(String fileName) throws IOException {
        return new AccessControlPacket(new ByteBufferKaitaiStream(fileName));
    }

    public AccessControlPacket(KaitaiStream _io) {
        this(_io, null, null);
    }

    public AccessControlPacket(KaitaiStream _io, KaitaiStruct _parent) {
        this(_io, _parent, null);
    }

    public AccessControlPacket(KaitaiStream _io, KaitaiStruct _parent, AccessControlPacket _root) {
        super(_io);
        this._parent = _parent;
        this._root = _root == null ? this : _root;
        _read();
    }
    private void _read() {
        this.frameCode = this._io.readU1();
        switch (frameCode()) {
            case 64: {
                this.payload = new DataFrame(this._io, this, _root);
                break;
            }
            case 48: {
                this.payload = new KeepAliveFrame(this._io, this, _root);
                break;
            }
            default: {
                this.payload = new UnsupportedFrame(this._io, this, _root);
                break;
            }
        }
    }
    public static class UnsupportedFrame extends KaitaiStruct {
        public static UnsupportedFrame fromFile(String fileName) throws IOException {
            return new UnsupportedFrame(new ByteBufferKaitaiStream(fileName));
        }

        public UnsupportedFrame(KaitaiStream _io) {
            this(_io, null, null);
        }

        public UnsupportedFrame(KaitaiStream _io, AccessControlPacket _parent) {
            this(_io, _parent, null);
        }

        public UnsupportedFrame(KaitaiStream _io, AccessControlPacket _parent, AccessControlPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.unsupportedData = this._io.readBytesFull();
        }
        private byte[] unsupportedData;
        private AccessControlPacket _root;
        private AccessControlPacket _parent;

        /**
         * Unsupported data payload
         */
        public byte[] unsupportedData() { return unsupportedData; }
        public AccessControlPacket _root() { return _root; }
        public AccessControlPacket _parent() { return _parent; }
    }
    public static class KeepAliveChannelStatesBits extends KaitaiStruct {
        public static KeepAliveChannelStatesBits fromFile(String fileName) throws IOException {
            return new KeepAliveChannelStatesBits(new ByteBufferKaitaiStream(fileName));
        }

        public KeepAliveChannelStatesBits(KaitaiStream _io) {
            this(_io, null, null);
        }

        public KeepAliveChannelStatesBits(KaitaiStream _io, KeepAliveFrame _parent) {
            this(_io, _parent, null);
        }

        public KeepAliveChannelStatesBits(KaitaiStream _io, KeepAliveFrame _parent, AccessControlPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.appFlag10Hidden = this._io.readBitsIntBe(1) != 0;
            this.appFlag11Hidden = this._io.readBitsIntBe(1) != 0;
            this.appFlag12Hidden = this._io.readBitsIntBe(1) != 0;
            this.appFlag13Hidden = this._io.readBitsIntBe(1) != 0;
            this.channel4CurrentState = this._io.readBitsIntBe(1) != 0;
            this.channel3CurrentState = this._io.readBitsIntBe(1) != 0;
            this.channel2CurrentState = this._io.readBitsIntBe(1) != 0;
            this.channel1CurrentState = this._io.readBitsIntBe(1) != 0;
        }
        private boolean appFlag10Hidden;
        private boolean appFlag11Hidden;
        private boolean appFlag12Hidden;
        private boolean appFlag13Hidden;
        private boolean channel4CurrentState;
        private boolean channel3CurrentState;
        private boolean channel2CurrentState;
        private boolean channel1CurrentState;
        private AccessControlPacket _root;
        private KeepAliveFrame _parent;
        public boolean appFlag10Hidden() { return appFlag10Hidden; }
        public boolean appFlag11Hidden() { return appFlag11Hidden; }
        public boolean appFlag12Hidden() { return appFlag12Hidden; }
        public boolean appFlag13Hidden() { return appFlag13Hidden; }
        public boolean channel4CurrentState() { return channel4CurrentState; }
        public boolean channel3CurrentState() { return channel3CurrentState; }
        public boolean channel2CurrentState() { return channel2CurrentState; }
        public boolean channel1CurrentState() { return channel1CurrentState; }
        public AccessControlPacket _root() { return _root; }
        public KeepAliveFrame _parent() { return _parent; }
    }
    public static class StatusByteBits extends KaitaiStruct {
        public static StatusByteBits fromFile(String fileName) throws IOException {
            return new StatusByteBits(new ByteBufferKaitaiStream(fileName));
        }

        public StatusByteBits(KaitaiStream _io) {
            this(_io, null, null);
        }

        public StatusByteBits(KaitaiStream _io, KaitaiStruct _parent) {
            this(_io, _parent, null);
        }

        public StatusByteBits(KaitaiStream _io, KaitaiStruct _parent, AccessControlPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.frameCounter = this._io.readBitsIntBe(3);
            this.appFlag2Hidden = this._io.readBitsIntBe(1) != 0;
            this.appFlag1Hidden = this._io.readBitsIntBe(1) != 0;
            this.timestampStatusHidden = this._io.readBitsIntBe(1) != 0;
            this.lowBat = this._io.readBitsIntBe(1) != 0;
            this.config = this._io.readBitsIntBe(1) != 0;
        }
        private long frameCounter;
        private boolean appFlag2Hidden;
        private boolean appFlag1Hidden;
        private boolean timestampStatusHidden;
        private boolean lowBat;
        private boolean config;
        private AccessControlPacket _root;
        private KaitaiStruct _parent;
        public long frameCounter() { return frameCounter; }
        public boolean appFlag2Hidden() { return appFlag2Hidden; }
        public boolean appFlag1Hidden() { return appFlag1Hidden; }
        public boolean timestampStatusHidden() { return timestampStatusHidden; }
        public boolean lowBat() { return lowBat; }
        public boolean config() { return config; }
        public AccessControlPacket _root() { return _root; }
        public KaitaiStruct _parent() { return _parent; }
    }
    public static class KeepAliveFrame extends KaitaiStruct {
        public static KeepAliveFrame fromFile(String fileName) throws IOException {
            return new KeepAliveFrame(new ByteBufferKaitaiStream(fileName));
        }

        public KeepAliveFrame(KaitaiStream _io) {
            this(_io, null, null);
        }

        public KeepAliveFrame(KaitaiStream _io, AccessControlPacket _parent) {
            this(_io, _parent, null);
        }

        public KeepAliveFrame(KaitaiStream _io, AccessControlPacket _parent, AccessControlPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.statusByte = new StatusByteBits(this._io, this, _root);
            this.channel1Info = this._io.readU2be();
            this.channel2Info = this._io.readU2be();
            this.channel3Info = this._io.readU2be();
            this.channel4Info = this._io.readU2be();
            this.channelStates = new KeepAliveChannelStatesBits(this._io, this, _root);
        }
        private StatusByteBits statusByte;
        private int channel1Info;
        private int channel2Info;
        private int channel3Info;
        private int channel4Info;
        private KeepAliveChannelStatesBits channelStates;
        private AccessControlPacket _root;
        private AccessControlPacket _parent;

        /**
         * Status byte
         */
        public StatusByteBits statusByte() { return statusByte; }

        /**
         * Channel 1 info (global event counter if configured in input mode)
         */
        public int channel1Info() { return channel1Info; }

        /**
         * Channel 2 info (global event counter if configured in input mode)
         */
        public int channel2Info() { return channel2Info; }

        /**
         * Channel 3 info (global event counter if configured in input mode)
         */
        public int channel3Info() { return channel3Info; }

        /**
         * Channel 4 info (global event counter if configured in input mode)
         */
        public int channel4Info() { return channel4Info; }

        /**
         * Keep Alive channel states (define precisely the input/output state)
         */
        public KeepAliveChannelStatesBits channelStates() { return channelStates; }
        public AccessControlPacket _root() { return _root; }
        public AccessControlPacket _parent() { return _parent; }
    }
    public static class DataChannelStatesBits extends KaitaiStruct {
        public static DataChannelStatesBits fromFile(String fileName) throws IOException {
            return new DataChannelStatesBits(new ByteBufferKaitaiStream(fileName));
        }

        public DataChannelStatesBits(KaitaiStream _io) {
            this(_io, null, null);
        }

        public DataChannelStatesBits(KaitaiStream _io, DataFrame _parent) {
            this(_io, _parent, null);
        }

        public DataChannelStatesBits(KaitaiStream _io, DataFrame _parent, AccessControlPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.channel4PreviousState = this._io.readBitsIntBe(1) != 0;
            this.channel4CurrentState = this._io.readBitsIntBe(1) != 0;
            this.channel3PreviousState = this._io.readBitsIntBe(1) != 0;
            this.channel3CurrentState = this._io.readBitsIntBe(1) != 0;
            this.channel2PreviousState = this._io.readBitsIntBe(1) != 0;
            this.channel2CurrentState = this._io.readBitsIntBe(1) != 0;
            this.channel1PreviousState = this._io.readBitsIntBe(1) != 0;
            this.channel1CurrentState = this._io.readBitsIntBe(1) != 0;
        }
        private boolean channel4PreviousState;
        private boolean channel4CurrentState;
        private boolean channel3PreviousState;
        private boolean channel3CurrentState;
        private boolean channel2PreviousState;
        private boolean channel2CurrentState;
        private boolean channel1PreviousState;
        private boolean channel1CurrentState;
        private AccessControlPacket _root;
        private DataFrame _parent;
        public boolean channel4PreviousState() { return channel4PreviousState; }
        public boolean channel4CurrentState() { return channel4CurrentState; }
        public boolean channel3PreviousState() { return channel3PreviousState; }
        public boolean channel3CurrentState() { return channel3CurrentState; }
        public boolean channel2PreviousState() { return channel2PreviousState; }
        public boolean channel2CurrentState() { return channel2CurrentState; }
        public boolean channel1PreviousState() { return channel1PreviousState; }
        public boolean channel1CurrentState() { return channel1CurrentState; }
        public AccessControlPacket _root() { return _root; }
        public DataFrame _parent() { return _parent; }
    }
    public static class DataFrame extends KaitaiStruct {
        public static DataFrame fromFile(String fileName) throws IOException {
            return new DataFrame(new ByteBufferKaitaiStream(fileName));
        }

        public DataFrame(KaitaiStream _io) {
            this(_io, null, null);
        }

        public DataFrame(KaitaiStream _io, AccessControlPacket _parent) {
            this(_io, _parent, null);
        }

        public DataFrame(KaitaiStream _io, AccessControlPacket _parent, AccessControlPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }
        private void _read() {
            this.statusByte = new StatusByteBits(this._io, this, _root);
            this.channel1Info = this._io.readU2be();
            this.channel2Info = this._io.readU2be();
            this.channel3Info = this._io.readU2be();
            this.channel4Info = this._io.readU2be();
            this.channelStates = new DataChannelStatesBits(this._io, this, _root);
        }
        private StatusByteBits statusByte;
        private int channel1Info;
        private int channel2Info;
        private int channel3Info;
        private int channel4Info;
        private DataChannelStatesBits channelStates;
        private AccessControlPacket _root;
        private AccessControlPacket _parent;

        /**
         * Status byte
         */
        public StatusByteBits statusByte() { return statusByte; }

        /**
         * Channel 1 info (event counter if configured in input mode, current output state if configured in output mode)
         */
        public int channel1Info() { return channel1Info; }

        /**
         * Channel 2 info (event counter if configured in input mode, current output state if configured in output mode)
         */
        public int channel2Info() { return channel2Info; }

        /**
         * Channel 3 info (event counter if configured in input mode, current output state if configured in output mode)
         */
        public int channel3Info() { return channel3Info; }

        /**
         * Channel 4 info (event counter if configured in input mode, current output state if configured in output mode)
         */
        public int channel4Info() { return channel4Info; }

        /**
         * Data channel states (define precisely the input/output state)
         */
        public DataChannelStatesBits channelStates() { return channelStates; }
        public AccessControlPacket _root() { return _root; }
        public AccessControlPacket _parent() { return _parent; }
    }
    private int frameCode;
    private KaitaiStruct payload;
    private AccessControlPacket _root;
    private KaitaiStruct _parent;

    /**
     * Frame code
     */
    public int frameCode() { return frameCode; }
    public KaitaiStruct payload() { return payload; }
    public AccessControlPacket _root() { return _root; }
    public KaitaiStruct _parent() { return _parent; }
}
