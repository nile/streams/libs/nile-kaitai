package ch.cern.nile.kaitai.streams;

import java.util.HashMap;
import java.util.Map;

import ch.cern.nile.kaitai.generated.CrackSensorsPacket;

/**
 * Test stream for testing the LoraDecoderStream.
 */
@SuppressWarnings("PMD.TestClassWithoutTestCases")
class TestStream extends LoraDecoderStream<CrackSensorsPacket> {

    private static final Map<String, String> CUSTOM_METADATA_FIELD_MAP = new HashMap<>();

    static {
        CUSTOM_METADATA_FIELD_MAP.put("adr", "adr");
        CUSTOM_METADATA_FIELD_MAP.put("temperature", "");
        CUSTOM_METADATA_FIELD_MAP.put("batteryVoltage", "battery_voltage");
    }

    TestStream() {
        super(CrackSensorsPacket.class, CUSTOM_METADATA_FIELD_MAP, true, true, true);
    }

}
