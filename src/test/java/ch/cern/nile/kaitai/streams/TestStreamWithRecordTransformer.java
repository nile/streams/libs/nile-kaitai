package ch.cern.nile.kaitai.streams;

import ch.cern.nile.kaitai.generated.CrackSensorsPacket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Test stream for testing the LoraDecoderStream.
 */
@SuppressWarnings("PMD.TestClassWithoutTestCases")
class TestStreamWithRecordTransformer extends LoraDecoderStream<CrackSensorsPacket> {

    private static final Map<String, String> CUSTOM_METADATA_FIELD_MAP = new HashMap<>();

    static {
        CUSTOM_METADATA_FIELD_MAP.put("adr", "adr");
        CUSTOM_METADATA_FIELD_MAP.put("temperature", "");
        CUSTOM_METADATA_FIELD_MAP.put("batteryVoltage", "battery_voltage");
    }

    TestStreamWithRecordTransformer() {
        super(CrackSensorsPacket.class, CUSTOM_METADATA_FIELD_MAP, true, true, true,
                TestStreamWithRecordTransformer::doubleSameRecordFunc);
    }

    @SuppressWarnings("PMD.UnusedPrivateMethod")
    private static List<Map<String, Object>> doubleSameRecordFunc(final Map<String, Object> stringObjectMap) {
        final List<Map<String, Object>> result = new ArrayList<>();
        result.add(new HashMap<>(stringObjectMap));
        result.add(new HashMap<>(stringObjectMap));
        return result;
    }
}
