package ch.cern.nile.kaitai.streams;

import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;
import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;


class LoraDecoderStreamWithRecordTransformerTest extends StreamTestBase {

    private static final JsonObject RECORD_CORRECT = TestUtils.loadRecordAsJson("data/record_correct.json");

    @Override
    public TestStreamWithRecordTransformer createStreamInstance() {
        return new TestStreamWithRecordTransformer();
    }

    @Test
    void givenDataWithTransformerToDoubleSameRecord_whenDecoding_thenOutputTwoEqualRecords() {
        pipeRecord(RECORD_CORRECT);
        final ProducerRecord<String, JsonObject> outputRecord1 = readRecord();
        final ProducerRecord<String, JsonObject> outputRecord2 = readRecord();

        final JsonObject outputValue1 = outputRecord1.value();
        final JsonObject outputValue2 = outputRecord2.value();

        assertNotNull(outputValue1);
        assertNotNull(outputValue2);
        assertEquals(outputValue1, outputValue2);

        assertFalse(outputValue1.has(""));
        assertFalse(outputValue1.has("temperature"));
        assertTrue(outputValue1.has("FRAMETYPE"));
        assertEquals(3545.62, outputValue1.get("battery_voltage").getAsDouble());
        assertEquals(-112.710_335f, outputValue1.get("displacementRaw").getAsFloat());
        assertEquals(3.496_520_3f, outputValue1.get("displacementPhysical").getAsFloat());
        assertTrue(outputValue1.get("adr").getAsBoolean());
        assertNotNull(outputValue1.get("timestamp"));
        assertEquals("lora-BE-gm-asg-crack-sensors", outputValue1.get("applicationName").getAsString());
        assertEquals("gm-asg-crack-sensors-00002", outputValue1.get("deviceName").getAsString());

        final long expectedTimestamp = Instant.parse("2022-11-18T13:02:29.574529606Z").toEpochMilli();
        assertEquals(expectedTimestamp, outputValue1.get("maxSnrRxInfo_time").getAsLong());

        assertEquals(-112, outputValue1.get("maxSnrRxInfo_rssi").getAsInt());
        assertEquals(-13.8, outputValue1.get("maxSnrRxInfo_loRaSNR").getAsDouble());

        assertEquals(46.26196, outputValue1.get("maxSnrRxInfo_location_latitude").getAsDouble());
        assertEquals(6.05774, outputValue1.get("maxSnrRxInfo_location_longitude").getAsDouble());
        assertEquals(486, outputValue1.get("maxSnrRxInfo_location_altitude").getAsInt());
    }
}