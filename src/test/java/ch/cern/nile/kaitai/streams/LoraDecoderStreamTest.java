package ch.cern.nile.kaitai.streams;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.util.NoSuchElementException;

import com.google.gson.JsonObject;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;

import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;


class LoraDecoderStreamTest extends StreamTestBase {

    private static final JsonObject RECORD_CORRECT = TestUtils.loadRecordAsJson("data/record_correct.json");
    private static final JsonObject RECORD_NO_TIME = TestUtils.loadRecordAsJson("data/record_no_time.json");
    private static final JsonObject RECORD_NO_FREQ = TestUtils.loadRecordAsJson("data/record_no_freq.json");
    private static final JsonObject RECORD_NO_DATA = TestUtils.loadRecordAsJson("data/record_no_data.json");
    private static final JsonObject RECORD_INVALID_DATA = TestUtils.loadRecordAsJson("data/record_invalid_data.json");

    @Override
    public TestStream createStreamInstance() {
        return new TestStream();
    }

    @Test
    void givenCorrectData_whenDecoding_thenOutputRecordIsCreatedWithExpectedValues() {
        pipeRecord(RECORD_CORRECT);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        final JsonObject outputValue = outputRecord.value();

        assertNotNull(outputValue);

        assertFalse(outputValue.has(""));
        assertFalse(outputValue.has("temperature"));
        assertTrue(outputValue.has("FRAMETYPE"));
        assertEquals(3545.62, outputValue.get("battery_voltage").getAsDouble());
        assertEquals(-112.710_335f, outputValue.get("displacementRaw").getAsFloat());
        assertEquals(3.496_520_3f, outputValue.get("displacementPhysical").getAsFloat());
        assertTrue(outputValue.get("adr").getAsBoolean());
        assertNotNull(outputValue.get("timestamp"));
        assertEquals("lora-BE-gm-asg-crack-sensors", outputValue.get("applicationName").getAsString());
        assertEquals("gm-asg-crack-sensors-00002", outputValue.get("deviceName").getAsString());

        final long expectedTimestamp = Instant.parse("2022-11-18T13:02:29.574529606Z").toEpochMilli();
        assertEquals(expectedTimestamp, outputValue.get("maxSnrRxInfo_time").getAsLong());

        assertEquals(-112, outputValue.get("maxSnrRxInfo_rssi").getAsInt());
        assertEquals(-13.8, outputValue.get("maxSnrRxInfo_loRaSNR").getAsDouble());

        assertEquals(46.26196, outputValue.get("maxSnrRxInfo_location_latitude").getAsDouble());
        assertEquals(6.05774, outputValue.get("maxSnrRxInfo_location_longitude").getAsDouble());
        assertEquals(486, outputValue.get("maxSnrRxInfo_location_altitude").getAsInt());
    }

    @Test
    void givenDataWithMissingTimeInMaxSnrGateway_whenDecoding_thenMaxSnrRxInfoTimeIsSetToTimestamp() {
        pipeRecord(RECORD_NO_TIME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        final JsonObject outputValue = outputRecord.value();
        assertNotNull(outputValue);

        assertNotNull(outputValue.get("timestamp"));
        assertEquals(outputValue.get("timestamp").getAsLong(), outputValue.get("maxSnrRxInfo_time").getAsLong());
    }


    @Test
    void givenDataWithNullElement_whenProcessing_thenDataIsIgnored() {
        pipeRecord(RECORD_NO_FREQ);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();
        assertFalse(outputRecord.value().has("frequency"));
    }

    @Test
    void givenMissingData_whenDecoding_thenNoOutputRecordIsCreated() {
        pipeRecord(RECORD_NO_DATA);
        assertThrows(NoSuchElementException.class, this::readRecord);
    }

    @Test
    void givenInvalidData_whenDecoding_thenNoOutputRecordIsCreated() {
        pipeRecord(RECORD_INVALID_DATA);
        assertThrows(NoSuchElementException.class, this::readRecord);
    }

}
