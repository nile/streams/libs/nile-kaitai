package ch.cern.nile.kaitai.streams;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

import org.junit.jupiter.api.Test;

class CustomEnricherTest {
    private static final String ORIGINAL_FIELD_KEY = "originalField";
    private static final String CUSTOM_FIELD_KEY = "customField";
    private static final String VALUE = "value";

    @Test
    void givenJsonNullOrEmpty_whenEnrichingMapWithCustomMetadata_thenCustomFieldIsNotPresent() {
        final JsonObject metadataJson = new JsonObject();
        metadataJson.add(ORIGINAL_FIELD_KEY, JsonNull.INSTANCE);
        final Map<String, String> customFields = new HashMap<>();
        customFields.put(ORIGINAL_FIELD_KEY, CUSTOM_FIELD_KEY);

        final CustomEnricher customEnricher = new CustomEnricher(customFields);

        final Map<String, Object> inputMap = new HashMap<>();
        final Map<String, Object> enrichedMap = customEnricher.enrichMapWithCustomMetadata(inputMap, metadataJson);

        assertFalse(enrichedMap.containsKey(CUSTOM_FIELD_KEY));
    }

    @Test
    void givenJsonPrimitive_whenEnrichingMapWithCustomMetadata_thenCustomFieldHasPrimitiveValue() {
        final JsonObject metadataJson = new JsonObject();
        metadataJson.addProperty(ORIGINAL_FIELD_KEY, VALUE);
        final Map<String, String> customFields = new HashMap<>();
        customFields.put(ORIGINAL_FIELD_KEY, CUSTOM_FIELD_KEY);

        final CustomEnricher customEnricher = new CustomEnricher(customFields);

        final Map<String, Object> inputMap = new HashMap<>();
        final Map<String, Object> enrichedMap = customEnricher.enrichMapWithCustomMetadata(inputMap, metadataJson);

        assertEquals(String.format("\"%s\"", VALUE), enrichedMap.get(CUSTOM_FIELD_KEY).toString());
    }

    @Test
    void givenNonJsonPrimitive_whenEnrichingMapWithCustomMetadata_thenCustomFieldHasNonPrimitiveValue() {
        final JsonObject metadataJson = new JsonObject();
        final JsonObject value = new JsonObject();
        value.addProperty("key", VALUE);
        metadataJson.add(ORIGINAL_FIELD_KEY, value);
        final Map<String, String> customFields = new HashMap<>();
        customFields.put(ORIGINAL_FIELD_KEY, CUSTOM_FIELD_KEY);

        final CustomEnricher customEnricher = new CustomEnricher(customFields);

        final Map<String, Object> inputMap = new HashMap<>();
        final Map<String, Object> enrichedMap = customEnricher.enrichMapWithCustomMetadata(inputMap, metadataJson);

        assertEquals(value, enrichedMap.get(CUSTOM_FIELD_KEY));
    }

    @Test
    void givenFieldNameChange_whenEnrichingMapWithCustomMetadata_thenOriginalFieldIsRemoved() {
        final JsonObject metadataJson = new JsonObject();
        metadataJson.addProperty(ORIGINAL_FIELD_KEY, VALUE);
        final Map<String, String> customFields = new HashMap<>();
        customFields.put(ORIGINAL_FIELD_KEY, CUSTOM_FIELD_KEY);

        final CustomEnricher customEnricher = new CustomEnricher(customFields);

        final Map<String, Object> inputMap = new HashMap<>();
        final Map<String, Object> enrichedMap = customEnricher.enrichMapWithCustomMetadata(inputMap, metadataJson);

        assertFalse(enrichedMap.containsKey(ORIGINAL_FIELD_KEY));
    }
}
