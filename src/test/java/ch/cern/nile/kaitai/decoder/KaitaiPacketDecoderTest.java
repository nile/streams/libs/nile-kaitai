package ch.cern.nile.kaitai.decoder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;

import com.google.gson.JsonElement;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.exceptions.DecodingException;
import ch.cern.nile.kaitai.generated.AccessControlPacket;
import ch.cern.nile.kaitai.generated.ComputerCenterTempPacket;
import ch.cern.nile.kaitai.generated.CrackSensorsPacket;
import ch.cern.nile.test.utils.TestUtils;

class KaitaiPacketDecoderTest {

    private static final JsonElement CRACK_SENSORS_DATA = TestUtils.getDataAsJsonElement("AAc/3sLha7FAX8b9");
    private static final JsonElement ACCESS_CONTROL_DATA = TestUtils.getDataAsJsonElement("QCIAAQAAAAAAAA4");
    private static final JsonElement COMPUTER_CENTER_TEMP_DATA = TestUtils.getDataAsJsonElement("AWcBKQJoagNnAL8EAv/2");
    private static final JsonElement EMPTY_DATA = TestUtils.getDataAsJsonElement("");
    private static final JsonElement INVALID_DATA = TestUtils.getDataAsJsonElement("invalidBase64");

    @Test
    void givenValidData_whenDecoding_thenCorrectlyDecoded() {
        final Map<String, Object> packet = KaitaiPacketDecoder.decode(CRACK_SENSORS_DATA, CrackSensorsPacket.class);
        assertEquals(18.55, packet.get("temperature"));
        assertEquals(3545.62, packet.get("batteryVoltage"));
        assertEquals(-112.710_335f, packet.get("displacementRaw"));
        assertEquals(3.496_520_3f, packet.get("displacementPhysical"));
    }

    @Test
    void givenValidDataThatContainNestedKaitaiStruct_whenDecoding_thenCorrectlyDecoded() {
        final Map<String, Object> packet = KaitaiPacketDecoder.decode(ACCESS_CONTROL_DATA, AccessControlPacket.class);
        assertEquals(64, packet.get("frameCode"));
        assertEquals(1L, packet.get("frameCounter"));
        assertEquals(1, packet.get("channel1Info"));
        assertEquals(0, packet.get("channel2Info"));
        assertEquals(0, packet.get("channel3Info"));
        assertEquals(0, packet.get("channel4Info"));
        assertEquals(true, packet.get("channel1PreviousState"));
        assertEquals(true, packet.get("channel2PreviousState"));
        assertEquals(false, packet.get("channel3PreviousState"));
        assertEquals(false, packet.get("channel4PreviousState"));
        assertEquals(false, packet.get("channel1CurrentState"));
        assertEquals(true, packet.get("channel2CurrentState"));
        assertEquals(false, packet.get("channel3CurrentState"));
        assertEquals(false, packet.get("channel4CurrentState"));
        assertEquals(false, packet.get("config"));
        assertEquals(true, packet.get("lowBat"));
    }

    @Test
    void givenValidDataThatContainNestedList_whenDecoding_thenCorectlyDecode() {
        final Map<String, Object> packet =
                KaitaiPacketDecoder.decode(COMPUTER_CENTER_TEMP_DATA, ComputerCenterTempPacket.class);
        assertEquals(29.7, packet.get("temperature"));
        assertEquals(53.0, packet.get("relativeHumidity"));
        assertEquals(19.1, packet.get("dewPoint"));
        assertEquals(-0.1, packet.get("differentialPressure"));
    }

    @Test
    void givenValidDataThatContainNestedKaitai_whenDecodeWithFrameType_thenFrameTypeAddedAndCorrect() {
        final Map<String, Object> packet =
                KaitaiPacketDecoder.decode(ACCESS_CONTROL_DATA, AccessControlPacket.class, true);
        assertTrue(packet.containsKey("frameType"));
        assertEquals("DataFrame", packet.get("frameType"));
    }

    @Test
    void givenValidData_whenDecodeWithFrameTypeAndCustomField_thenFrameTypeAddedAndCorrect() {
        final Map<String, Object> packet =
                KaitaiPacketDecoder.decode(CRACK_SENSORS_DATA, CrackSensorsPacket.class, true);
        assertTrue(packet.containsKey("FRAMETYPE"));
        assertEquals("CrackSensorsPacket", packet.get("FRAMETYPE"));
    }

    @Test
    void givenInvalidBase64_whenDecoding_thenThrowsDecodingException() {
        assertThrows(DecodingException.class, () -> KaitaiPacketDecoder.decode(INVALID_DATA, CrackSensorsPacket.class));
    }

    @Test
    void givenEmptyDataFrame_whenDecoding_thenThrowsDecodingException() {
        assertThrows(DecodingException.class, () -> KaitaiPacketDecoder.decode(EMPTY_DATA, CrackSensorsPacket.class));
    }

    @Test
    void givenUnsupportedFrameType_whenDecodingPacket_thenThrowsDecodingException() {
        assertThrows(DecodingException.class,
                () -> KaitaiPacketDecoder.decode(ACCESS_CONTROL_DATA, AccessControlPacket.UnsupportedFrame.class));
    }

    @Test
    void givenMethodInvocationError_whenDecodingPacket_thenThrowsDecodingException() {
        assertThrows(DecodingException.class,
                () -> KaitaiPacketDecoder.decode(INVALID_DATA, AccessControlPacket.class));
    }

}
