package ch.cern.nile.kaitai.decoder;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ValueParserTest {
    @Test
    void givenInvalidNumericValue_whenParsingNumericValue_thenReturnsOriginalString() {
        final String invalidValue = "foo";
        final Object result = ValueParser.parse(invalidValue);
        assertEquals(invalidValue, result);
    }

    @Test
    void givenIntegerValue_whenParsingNumericValue_thenReturnsInteger() {
        assertEquals(10, ValueParser.parse("10"));
    }

    @Test
    void givenLongValue_whenParsingNumericValue_thenReturnsLong() {
        final long largeValue = Integer.MAX_VALUE + 1L;
        assertEquals(largeValue, ValueParser.parse(String.valueOf(largeValue)));
    }

    @Test
    void givenFloatValue_whenParsingNumericValue_thenReturnsFloat() {
        assertEquals(10.5, ValueParser.parse("10.5"));
    }

    @Test
    void givenNonNumericValue_whenParsingNumericValue_thenReturnsString() {
        assertEquals("test", ValueParser.parse("test"));
    }
}
