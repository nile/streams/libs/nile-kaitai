package ch.cern.nile.kaitai.decoder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.cern.nile.common.exceptions.DecodingException;

import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStream;
import io.kaitai.struct.KaitaiStruct;

/**
 * Instantiates a Kaitai Struct generated class.
 */
final class KaitaiStructInstantiator {

    private static final Logger LOGGER = LoggerFactory.getLogger(KaitaiStructInstantiator.class);

    private KaitaiStructInstantiator() {
    }

    /**
     * Instantiates a Kaitai Struct generated class.
     *
     * @param clazz       the class to instantiate
     * @param dataDecoded the decoded data to instantiate the class with
     * @param <T>         the type of the KaitaiStruct
     * @return the instantiated class
     */
    static <T extends KaitaiStruct> T instantiate(final Class<T> clazz, final byte[] dataDecoded) {
        try {
            final Constructor<T> constructor = clazz.getDeclaredConstructor(KaitaiStream.class);
            return constructor.newInstance(new ByteBufferKaitaiStream(dataDecoded));
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException
                 | NoSuchMethodException e) {
            final String message =
                    String.format("Could not instantiate Kaitai Struct generated class %s", clazz.getName());
            LOGGER.error(message, e);
            throw new DecodingException(message, e);
        }
    }
}
