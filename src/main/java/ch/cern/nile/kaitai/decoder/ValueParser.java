package ch.cern.nile.kaitai.decoder;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Parses the value to the appropriate type.
 */
final class ValueParser {

    private ValueParser() {
    }

    /**
     * Casts the value to the appropriate type.
     *
     * @param value the value to parse
     * @return the parsed value
     */
    @SuppressFBWarnings(value = "SECRD", justification = "This is a parser, it is supposed to parse any value")
    static Object parse(final Object value) {
        Object parsedValue = value;
        if (value instanceof String) {
            final String str = (String) value;
            if (str.matches("^[-+]?\\d+$")) {
                try {
                    parsedValue = Integer.parseInt(str);
                } catch (NumberFormatException e) {
                    parsedValue = Long.parseLong(str);
                }
            } else if (str.matches("^[-+]?\\d+(\\.\\d+)?([eE][-+]?\\d+)?$")) {
                parsedValue = Double.parseDouble(str);
            }

        }
        return parsedValue;
    }
}

