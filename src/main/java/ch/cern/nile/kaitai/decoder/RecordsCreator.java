package ch.cern.nile.kaitai.decoder;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import ch.cern.nile.common.exceptions.DecodingException;

import io.kaitai.struct.KaitaiStruct;

/**
 * Creates a map of records from a KaitaiStruct packet.
 */
final class RecordsCreator {

    private static final String UNSUPPORTED_FRAME_NAME = "UnsupportedFrame";

    private RecordsCreator() {
    }

    /**
     * Creates a map of records from a KaitaiStruct packet.
     *
     * @param packet       the KaitaiStruct packet
     * @param addFrameType add the frame type in the decoded map
     *                     (use with Kaitai structs that have different frame types)
     * @param <T>          the type of KaitaiStruct
     * @return a Map containing the decoded payload data
     */
    static <T extends KaitaiStruct> Map<String, Object> createRecordsMap(final T packet, final boolean addFrameType)
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        checkForUnsupportedFrame(packet);

        final Map<String, String> fieldNameMapping = FieldNameMapper.loadFieldNameMapping(
                packet.getClass().getDeclaringClass() != null ? packet.getClass().getDeclaringClass().getSimpleName() :
                        packet.getClass().getSimpleName());

        final Map<String, Object> records = new HashMap<>();

        if (addFrameType) {
            addFrameTypeToRecords(records, packet, fieldNameMapping);
        }

        processFields(packet, records, fieldNameMapping);
        return MapFlattener.flattenMap(records);
    }

    /**
     * Creates a map of records from a KaitaiStruct packet.
     * This method is used when the packet contains a payload, in order to include any possible fields from the original
     * packet.
     *
     * @param packet  the KaitaiStruct packet
     * @param payload the KaitaiStruct payload, used to filter out the fields from the already decoded payload
     * @param <T>     the type of KaitaiStruct
     * @return a Map containing the decoded original packet data
     */
    static <T extends KaitaiStruct> Map<String, Object> createPacketsFieldsMap(final T packet,
                                                                               final KaitaiStruct payload)
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        checkForUnsupportedFrame(packet);

        final Map<String, String> fieldNameMapping = FieldNameMapper.loadFieldNameMapping(
                packet.getClass().getDeclaringClass() != null ? packet.getClass().getDeclaringClass().getSimpleName() :
                        packet.getClass().getSimpleName());

        final Map<String, Object> records = new HashMap<>();
        processFields(packet, records, fieldNameMapping, payload);
        return MapFlattener.flattenMap(records);
    }


    private static void checkForUnsupportedFrame(final KaitaiStruct packet) {
        if (UNSUPPORTED_FRAME_NAME.equals(packet.getClass().getSimpleName())) {
            throw new DecodingException("Error while decoding packet: Unsupported frame");
        }
    }

    private static void addFrameTypeToRecords(final Map<String, Object> records, final KaitaiStruct packet,
                                              final Map<String, String> fieldNameMapping) {
        final String frameType = packet.getClass().getSimpleName();
        records.put(fieldNameMapping.getOrDefault("frameType", "frameType"), frameType);
    }

    private static void processFields(final KaitaiStruct packet, final Map<String, Object> records,
                                      final Map<String, String> fieldNameMapping)
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        for (final Field field : packet.getClass().getDeclaredFields()) {
            if (FieldNameMapper.shouldSkipField(field.getName())) {
                continue;
            }
            processField(packet, field, records, fieldNameMapping);
        }
    }

    private static <T extends KaitaiStruct> void processFields(final T packet, final Map<String, Object> records,
                                                               final Map<String, String> fieldNameMapping,
                                                               final KaitaiStruct payload)
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        for (final Field field : packet.getClass().getDeclaredFields()) {
            if (FieldNameMapper.shouldSkipField(field.getName())
                    || field.getName().equals(payload.getClass().getSimpleName())) {
                continue;
            }
            processField(packet, field, records, fieldNameMapping);
        }
    }

    private static void processField(final KaitaiStruct packet, final Field field,
                                     final Map<String, Object> records, final Map<String, String> fieldNameMapping)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        final String fieldName = fieldNameMapping.getOrDefault(field.getName(), field.getName());

        final Method getterMethod = packet.getClass().getMethod(
                field.getName()); // note that the getter method is not the same as the possibly mapped field name

        final Object value = getterMethod.invoke(packet);
        if (value != null) {
            processNonNullField(records, fieldName, value);
        } else {
            processNullField(packet, field, records, fieldName);
        }
    }


    private static void processNonNullField(final Map<String, Object> records, final String fieldName,
                                            final Object value)
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        if (value instanceof KaitaiStruct) {
            records.put(fieldName, createRecordsMap((KaitaiStruct) value, false));
        } else if (value instanceof String) {
            records.put(fieldName, ValueParser.parse(value));
        } else {
            records.put(fieldName, value);
        }
    }

    private static void processNullField(final KaitaiStruct packet, final Field field,
                                         final Map<String, Object> records, final String fieldName)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final Object fieldValue = packet.getClass().getDeclaredMethod(field.getName()).invoke(packet);
        if (fieldValue instanceof KaitaiStruct) {
            records.put(fieldName.replace("()", ""), createRecordsMap((KaitaiStruct) fieldValue, false));
        } else {
            records.put(fieldName.replace("()", ""), ValueParser.parse(fieldValue));
        }
    }

}
