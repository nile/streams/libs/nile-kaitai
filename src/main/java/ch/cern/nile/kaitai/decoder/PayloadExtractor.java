package ch.cern.nile.kaitai.decoder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.cern.nile.common.exceptions.DecodingException;

import io.kaitai.struct.KaitaiStruct;

/**
 * Optimized class for extracting the payload from a KaitaiStruct.
 */
final class PayloadExtractor {

    private static final Logger LOGGER = LoggerFactory.getLogger(PayloadExtractor.class);

    private PayloadExtractor() {
    }

    /**
     * Tries to extract the payload or root from a KaitaiStruct by invoking either the 'payload' or '_root' method.
     *
     * @param packet the KaitaiStruct to extract from
     * @param <T>    the type of the KaitaiStruct
     * @return the extracted KaitaiStruct payload or root
     * @throws DecodingException if the payload cannot be extracted
     */
    static <T extends KaitaiStruct> KaitaiStruct extract(final T packet) {
        for (final String methodName : new String[]{"payload", "_root"}) {
            try {
                final Method extractionMethod = packet.getClass().getMethod(methodName);
                return (KaitaiStruct) extractionMethod.invoke(packet);
            } catch (NoSuchMethodException ignored) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Method '{}' not found", methodName);
                }
            } catch (InvocationTargetException | IllegalAccessException cause) {
                final String message = String.format("Error invoking method '%s'", methodName);
                LOGGER.error(message, cause);
                throw new DecodingException(message, cause);
            }
        }

        final String message = "No suitable payload extraction method found ('payload' or '_root')";
        LOGGER.error(message);
        throw new DecodingException(message);
    }
}
