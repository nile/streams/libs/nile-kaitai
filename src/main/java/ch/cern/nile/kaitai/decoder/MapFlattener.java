package ch.cern.nile.kaitai.decoder;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.kaitai.struct.KaitaiStruct;

/**
 * Smart flattener for KaitaiStruct maps.
 */
final class MapFlattener {

    private MapFlattener() {
    }

    /**
     * Flattens a map generated from a KaitaiStruct.
     *
     * @param original the original map
     * @return the flattened map
     */
    static Map<String, Object> flattenMap(final Map<String, Object> original)
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        final Map<String, Object> flattened = new HashMap<>();
        for (final Map.Entry<String, Object> entry : original.entrySet()) {
            final Object value = entry.getValue();
            if (value instanceof Map) {
                final Map<String, Object> subMap = flattenMap((Map<String, Object>) value);
                flattened.putAll(subMap);
            } else if (value instanceof List) {
                flattenList(flattened, entry.getKey(), (List<Object>) value);
            } else if (value instanceof KaitaiStruct) {
                final Map<String, Object> nestedObject = RecordsCreator.createRecordsMap((KaitaiStruct) value, false);
                flattened.putAll(nestedObject);
            } else {
                flattened.put(entry.getKey(), value);
            }
        }
        return flattened;
    }

    private static void flattenList(final Map<String, Object> flattened, final String key, final List<Object> list)
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        for (final Object value : list) {
            if (value instanceof Map) {
                final Map<String, Object> subMap = flattenMap((Map<String, Object>) value);
                flattened.putAll(subMap);
            } else if (value instanceof KaitaiStruct) {
                final Map<String, Object> nestedObject = RecordsCreator.createRecordsMap((KaitaiStruct) value, false);
                flattened.putAll(nestedObject);
            } else {
                flattened.put(key, value);
            }
        }
    }
}
