package ch.cern.nile.kaitai.decoder;

import java.lang.reflect.InvocationTargetException;
import java.util.Base64;
import java.util.Map;

import com.google.gson.JsonElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.cern.nile.common.exceptions.DecodingException;

import io.kaitai.struct.KaitaiStruct;

/**
 * A class that decodes a KaitaiStruct packet from a Base64 encoded payload.
 */
public final class KaitaiPacketDecoder {

    private static final Logger LOGGER = LoggerFactory.getLogger(KaitaiPacketDecoder.class);

    private KaitaiPacketDecoder() {
    }

    /**
     * Decodes the payload of a Kaitai Struct generated class into a map.
     *
     * @param <T>   the type of KaitaiStruct
     * @param data  the payload data in Base64 encoding as a JsonElement
     * @param clazz the Kaitai Struct generated class of type T
     * @return a Map containing the decoded payload data, with field names as keys and their respective values as map
     * values
     */
    public static <T extends KaitaiStruct> Map<String, Object> decode(final JsonElement data, final Class<T> clazz) {
        return decode(data, clazz, false);
    }

    /**
     * Decodes the payload of a Kaitai Struct generated class into a map.
     *
     * @param <T>          the type of KaitaiStruct
     * @param data         the payload data in Base64 encoding as a JsonElement
     * @param clazz        the Kaitai Struct generated class of type T
     * @param addFrameType add the frame type in the decoded map
     *                     (use with Kaitai structs that have different frame types)
     * @return a Map containing the decoded payload data
     */
    public static <T extends KaitaiStruct> Map<String, Object> decode(final JsonElement data, final Class<T> clazz,
                                                                      final boolean addFrameType) {
        final byte[] dataDecoded;

        try {
            dataDecoded = Base64.getDecoder().decode(data.getAsString());
        } catch (IllegalArgumentException e) {
            final String errorMessage = "Error while decoding Base64 data";
            LOGGER.error(errorMessage, e);
            throw new DecodingException(errorMessage, e);
        }

        final T packet = KaitaiStructInstantiator.instantiate(clazz, dataDecoded);
        final KaitaiStruct payload = PayloadExtractor.extract(packet);

        Map<String, Object> records;
        try {
            if (payload == null) {
                // If the payload is null, the packet is the payload
                records = RecordsCreator.createRecordsMap(packet, addFrameType);
            } else {
                // If the payload is not null, the packet is a wrapper and the payload is the packet
                records = RecordsCreator.createRecordsMap(payload, addFrameType);
                // The packet might contain fields that are not in the payload (e.g. frameType)
                records.putAll(RecordsCreator.createPacketsFieldsMap(packet, payload));
            }
        } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
            final String errorMessage = "Error while creating records map";
            LOGGER.error(errorMessage, e);
            throw new DecodingException(errorMessage, e);
        }
        return records;

    }
}
