package ch.cern.nile.kaitai.decoder;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loads the field name mapping from the JSON file.
 */
final class FieldNameMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(FieldNameMapper.class);

    private static final Set<String> FIELDS_TO_SKIP =
            new HashSet<>(Arrays.asList("_io", "_parent", "_root", "_raw_packet", "magic"));

    private FieldNameMapper() {
    }

    /**
     * Loads the field name mapping from the JSON file.
     *
     * @param className the name of the class to load the mapping for
     * @return the field name mapping
     */
    static Map<String, String> loadFieldNameMapping(final String className) {
        final Gson gson = new Gson();
        final String fileName = "/kaitai_field_name_mappings/" + className + ".json";
        Map<String, String> fieldNameMapping = new HashMap<>();

        try (InputStream inputStream = KaitaiPacketDecoder.class.getResourceAsStream(fileName)) {
            if (inputStream != null) {
                try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
                    final Type mapType = new TypeToken<Map<String, String>>() {
                    }.getType();
                    fieldNameMapping = gson.fromJson(reader, mapType);
                }
            }
        } catch (IOException e) {
            // No mapping file found, will use field names as is
            LOGGER.info("No field name mapping file found for class {}", className);
        }

        return fieldNameMapping;
    }


    /**
     * Skips fields that are not needed in the output map.
     *
     * @param fieldName the name of the field to check
     * @return true if the field should be skipped, false otherwise
     */
    static boolean shouldSkipField(final String fieldName) {
        return fieldName.endsWith("Hidden") || FIELDS_TO_SKIP.contains(fieldName);
    }
}
