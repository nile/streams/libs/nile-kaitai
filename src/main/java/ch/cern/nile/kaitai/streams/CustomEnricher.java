package ch.cern.nile.kaitai.streams;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Enriches a map with custom metadata fields based on the provided metadata JSON.
 */
public class CustomEnricher {
    private final Map<String, String> customMetadataFields;

    /**
     * Constructor for the CustomEnricher.
     *
     * @param customMetadataFields the map of custom metadata fields to enrich
     */
    public CustomEnricher(final Map<String, String> customMetadataFields) {
        this.customMetadataFields = new HashMap<>(customMetadataFields);
    }

    /**
     * Enriches the input map with custom metadata fields based on the provided metadata JSON.
     * Iterates through the custom metadata fields and processes each field individually.
     *
     * @param inputMap     the original map to be enriched
     * @param metadataJson the JSON object containing the metadata
     * @return The enriched map
     */
    public Map<String, Object> enrichMapWithCustomMetadata(final Map<String, Object> inputMap,
                                                           final JsonObject metadataJson) {
        final Map<String, Object> enrichedMap = new HashMap<>(inputMap);
        for (final Map.Entry<String, String> customFieldEntry : customMetadataFields.entrySet()) {
            processCustomField(metadataJson, enrichedMap, customFieldEntry);
        }
        return enrichedMap;
    }

    /**
     * Processes a single custom field entry. Determines whether to enrich or transfer/remove the field.
     *
     * @param metadataJson     the JSON object containing the metadata
     * @param enrichedMap      the map being enriched
     * @param customFieldEntry the entry of the custom field to process
     */
    private void processCustomField(final JsonObject metadataJson, final Map<String, Object> enrichedMap,
                                    final Map.Entry<String, String> customFieldEntry) {
        final String metadataValueName = customFieldEntry.getKey();
        final String customFieldName = customFieldEntry.getValue();

        if (customFieldName.isEmpty()) {
            enrichedMap.remove(metadataValueName);
        } else if (metadataJson.has(metadataValueName)) {
            final JsonElement fieldValue = metadataJson.get(metadataValueName);
            enrichField(fieldValue, enrichedMap, metadataValueName, customFieldName);
        } else {
            transferOrRemoveField(enrichedMap, metadataValueName, customFieldName);
        }
    }

    /**
     * Enriches the map with a specific field value. Handles empty and non-empty JsonElements.
     *
     * @param fieldValue        the value of the field to enrich
     * @param enrichedMap       the map being enriched
     * @param metadataValueName the original field name in the metadata
     * @param customFieldName   the new field name to use in the enriched map
     */
    private void enrichField(final JsonElement fieldValue, final Map<String, Object> enrichedMap,
                             final String metadataValueName, final String customFieldName) {
        if (fieldValue.isJsonNull() || isPrimitiveAndEmpty(fieldValue)) {
            enrichedMap.remove(customFieldName);
        } else {
            enrichedMap.put(customFieldName, fieldValue);
            if (!metadataValueName.equals(customFieldName)) {
                enrichedMap.remove(metadataValueName);
            }
        }
    }

    /**
     * Checks if the given JsonElement is a primitive and empty.
     *
     * @param fieldValue the JsonElement to check
     * @return true if the element is a primitive and empty, false otherwise
     */
    private boolean isPrimitiveAndEmpty(final JsonElement fieldValue) {
        return fieldValue.isJsonPrimitive() && fieldValue.getAsString().isEmpty();
    }

    /**
     * Transfers or removes a field from the map based on the field names.
     * If the original and custom field names are different, the original field is removed.
     *
     * @param enrichedMap       the map being modified
     * @param metadataValueName the original field name
     * @param customFieldName   the new field name to use
     */
    private void transferOrRemoveField(final Map<String, Object> enrichedMap, final String metadataValueName,
                                       final String customFieldName) {
        if (enrichedMap.containsKey(metadataValueName) && !metadataValueName.equals(customFieldName)) {
            final Object originalValue = enrichedMap.get(metadataValueName);
            enrichedMap.put(customFieldName, originalValue);
            enrichedMap.remove(metadataValueName);
        }
    }
}
