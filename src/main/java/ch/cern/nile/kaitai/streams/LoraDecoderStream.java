package ch.cern.nile.kaitai.streams;

import java.text.ParseException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.cern.nile.common.json.JsonSerde;
import ch.cern.nile.common.streams.AbstractStream;
import ch.cern.nile.common.streams.StreamUtils;
import ch.cern.nile.common.streams.offsets.InjectOffsetProcessorSupplier;
import ch.cern.nile.kaitai.decoder.KaitaiPacketDecoder;

import io.kaitai.struct.KaitaiStruct;

/**
 * This abstract class extends the AbstractStream class and is parameterized with a type that extends KaitaiStruct.
 * KaitaiStruct is a class from the Kaitai Struct framework,
 * which is a declarative language used for describing various binary data structures.
 * This class provides a common structure for decoding streams of data that represented by a KaitaiStruct.
 * The specific type of KaitaiStruct that this stream will handle is determined by the type parameter T.
 *
 * @param <T> The type of KaitaiStruct that this stream will handle.
 */
public abstract class LoraDecoderStream<T extends KaitaiStruct> extends AbstractStream {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoraDecoderStream.class.getName());
    public static final String BATMON_STREAM_CLASS_NAME = "BatmonStream";
    private final Class<T> packetClass;
    private final CustomEnricher customEnricher;
    private final Function<Map<String, Object>, List<Map<String, Object>>> recordTransformer;
    private final boolean addFrameType;
    private final boolean addGatewayDetails;
    private final boolean addTxDetails;

    /**
     * Constructor for the LoraDecoderStream.
     *
     * @param packetClass          the class of the KaitaiStruct that this stream will handle
     * @param customMetadataFields the custom metadata fields to be added to the map
     * @param addFrameType         whether to add the frame type to the map
     * @param addGatewayDetails    whether to add the gateway details to the map
     * @param addTxDetails         whether to add the tx details to the map
     * @param recordTransformer    function used for transforming a record or splitting it into multiple ones
     */
    public LoraDecoderStream(final Class<T> packetClass, final Map<String, String> customMetadataFields,
                             final boolean addFrameType, final boolean addGatewayDetails, final boolean addTxDetails,
                             final Function<Map<String, Object>, List<Map<String, Object>>> recordTransformer) {
        this.packetClass = packetClass;
        this.customEnricher = new CustomEnricher(customMetadataFields);
        this.addFrameType = addFrameType;
        this.addGatewayDetails = addGatewayDetails;
        this.addTxDetails = addTxDetails;
        this.recordTransformer = recordTransformer;

    }

    /**
     * Constructor for the LoraDecoderStream.
     *
     * @param packetClass          the class of the KaitaiStruct that this stream will handle
     * @param customMetadataFields the custom metadata fields to be added to the map
     * @param addFrameType         whether to add the frame type to the map
     * @param addGatewayDetails    whether to add the gateway details to the map
     * @param addTxDetails         whether to add the tx details to the map
     */
    public LoraDecoderStream(final Class<T> packetClass, final Map<String, String> customMetadataFields,
                             final boolean addFrameType, final boolean addGatewayDetails, final boolean addTxDetails) {
        this(packetClass, customMetadataFields, addFrameType, addGatewayDetails, addTxDetails,
                Collections::singletonList);
    }

    /**
     * Constructor for the LoraDecoderStream.
     * The default value for customMetadataFields is an empty map.
     *
     * @param packetClass       the class of the KaitaiStruct that this stream will handle
     * @param addFrameType      whether to add the frame type to the map
     * @param addGatewayDetails whether to add the gateway details to the map
     * @param addTxDetails      whether to add the tx details to the map
     */
    public LoraDecoderStream(final Class<T> packetClass,
                             final boolean addFrameType, final boolean addGatewayDetails, final boolean addTxDetails) {
        this(packetClass, Collections.emptyMap(), addFrameType, addGatewayDetails, addTxDetails,
                Collections::singletonList);
    }

    /**
     * Constructor for the LoraDecoderStream.
     * The default values for addFrameType, addGatewayDetails and addTxDetails are false.
     *
     * @param packetClass the class of the KaitaiStruct that this stream will handle
     */
    public LoraDecoderStream(final Class<T> packetClass) {
        this(packetClass, Collections.emptyMap(), false, false, false,
                Collections::singletonList);
    }

    /**
     * Custom enrichment to be applied to the map.
     *
     * @param inputMap     the map to enrich
     * @param metadataJson the value to enrich the map with
     * @return the inputMap with no enrichment (in case this method is not overriden)
     */
    public Map<String, Object> enrichCustomFunction(final Map<String, Object> inputMap, final JsonObject metadataJson) {
        // Override this method to add custom enrichment
        return inputMap;
    }

    /**
     * Creates a topology for the stream.
     *
     * @param builder the streams' builder
     */
    @Override
    public void createTopology(final StreamsBuilder builder) {
        builder.stream(getSourceTopic(), Consumed.with(Serdes.String(), new JsonSerde()))
                .filter(StreamUtils::filterRecord)
                .processValues(new InjectOffsetProcessorSupplier(), InjectOffsetProcessorSupplier.getSTORE_NAME())
                .mapValues(this::mapValues)
                .filter(StreamUtils::filterNull)
                .filter(StreamUtils::filterEmpty)
                .flatMapValues(this.recordTransformer::apply)
                .to(getSinkTopic());
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    private Map<String, Object> mapValues(final JsonObject metadataJson) {
        Map<String, Object> enrichedMap;
        try {
            final Map<String, Object> map =
                    KaitaiPacketDecoder.decode(metadataJson.get("data"), packetClass, addFrameType);
            enrichedMap = enrichMapWithCommonFields(map, metadataJson);
            enrichedMap = customEnricher.enrichMapWithCustomMetadata(enrichedMap, metadataJson);
            enrichedMap = enrichCustomFunction(enrichedMap, metadataJson);
            if (addGatewayDetails) {
                enrichedMap = enrichMapWithGatewayDetails(enrichedMap, metadataJson.get("rxInfo").getAsJsonArray());
            }
            if (addTxDetails) {
                enrichedMap = enrichMapWithTxDetails(enrichedMap, metadataJson.get("txInfo").getAsJsonObject());
            }
        } catch (RuntimeException | ParseException e) {
            logStreamsException(e);
            enrichedMap = Collections.emptyMap();
        } finally {
            setLastReadOffset(metadataJson.get("offset").getAsLong());
        }
        return enrichedMap;
    }

    private Map<String, Object> enrichMapWithCommonFields(final Map<String, Object> inputMap,
                                                          final JsonObject metadataJson) throws ParseException {
        final Map<String, Object> enrichedMap = new HashMap<>(inputMap);
        StreamUtils.addTimestamp(metadataJson.get("rxInfo").getAsJsonArray(), enrichedMap);
        enrichedMap.put("applicationID", metadataJson.get("applicationID").getAsString());
        enrichedMap.put("applicationName", metadataJson.get("applicationName").getAsString());
        enrichedMap.put("deviceName", metadataJson.get("deviceName").getAsString());
        enrichedMap.put("devEUI", metadataJson.get("devEUI").getAsString());
        return enrichedMap;
    }

    private Map<String, Object> enrichMapWithGatewayDetails(final Map<String, Object> inputMap,
                                                            final JsonArray rxInfo) {
        final Map<String, Object> enrichedMap = new HashMap<>(inputMap);
        double maxSnr = Double.NEGATIVE_INFINITY;
        JsonObject maxSnrRxInfo = null;
        for (int i = 0; i < rxInfo.size(); i++) {
            final JsonObject currentInfo = rxInfo.get(i).getAsJsonObject();
            final JsonElement snr = currentInfo.get("loRaSNR");
            if (snr != null && !snr.isJsonNull()) {
                final double snrValue = snr.getAsDouble();
                if (snrValue > maxSnr) {
                    maxSnr = snrValue;
                    maxSnrRxInfo = currentInfo;
                }
            }
        }
        if (maxSnrRxInfo != null) {
            putAsStringIfNotNullOrJsonNull(enrichedMap, maxSnrRxInfo, "gatewayID", "maxSnrRxInfo_gatewayID");
            putAsStringIfNotNullOrJsonNull(enrichedMap, maxSnrRxInfo, "uplinkID", "maxSnrRxInfo_uplinkID");
            putAsStringIfNotNullOrJsonNull(enrichedMap, maxSnrRxInfo, "name", "maxSnrRxInfo_name");

            final JsonElement timeElement = maxSnrRxInfo.get("time");

            enrichedMap.put("maxSnrRxInfo_time", getMaxSnrTimestamp(enrichedMap, timeElement));

            putAsStringIfNotNullOrJsonNull(enrichedMap, maxSnrRxInfo, "rssi", "maxSnrRxInfo_rssi");
            putAsStringIfNotNullOrJsonNull(enrichedMap, maxSnrRxInfo, "loRaSNR", "maxSnrRxInfo_loRaSNR");

            final JsonObject location = maxSnrRxInfo.getAsJsonObject("location");
            if (location != null) {
                putAsStringIfNotNullOrJsonNull(enrichedMap, location, "latitude", "maxSnrRxInfo_location_latitude");
                putAsStringIfNotNullOrJsonNull(enrichedMap, location, "longitude", "maxSnrRxInfo_location_longitude");
                putAsStringIfNotNullOrJsonNull(enrichedMap, location, "altitude", "maxSnrRxInfo_location_altitude");
            }
        }
        return enrichedMap;
    }

    private Map<String, Object> enrichMapWithTxDetails(final Map<String, Object> inputMap, final JsonObject txInfo) {
        final Map<String, Object> enrichedMap = new HashMap<>(inputMap);
        putIfNotNullOrJsonNull(enrichedMap, txInfo, "frequency", "txInfo_frequency");
        putIfNotNullOrJsonNull(enrichedMap, txInfo, "dr", "txInfo_dr");
        return enrichedMap;
    }

    private void putAsStringIfNotNullOrJsonNull(final Map<String, Object> output, final JsonObject jsonObject,
                                                final String key, final String outputKey) {
        final JsonElement element = jsonObject.get(key);
        if (element != null && !element.isJsonNull()) {
            output.put(outputKey, element.getAsString());
        } else {
            LOGGER.warn("Element {} is null or JsonNull", key);
        }
    }

    private void putIfNotNullOrJsonNull(final Map<String, Object> output, final JsonObject jsonObject, final String key,
                                        final String outputKey) {
        final JsonElement element = jsonObject.get(key);
        if (element != null && !element.isJsonNull()) {
            output.put(outputKey, element);
        } else {
            LOGGER.warn("Element {} is null or JsonNull", key);
        }
    }

    private Object getMaxSnrTimestamp(final Map<String, Object> enrichedMap,
                                      final JsonElement timeElement) {
        final Object time;
        if (BATMON_STREAM_CLASS_NAME.equals(this.getClass().getSimpleName())) {
            // BatmonStream is a special case because historically it has been using a different timestamp format
            if (timeElement != null && !timeElement.isJsonNull()) {
                time = timeElement.getAsString();
            } else {
                time = timestampToString(Long.parseLong(enrichedMap.get("timestamp").toString()));
            }
        } else {
            if (timeElement != null && !timeElement.isJsonNull()) {
                time = Instant.parse(timeElement.getAsString()).toEpochMilli();
            } else {
                time = enrichedMap.get("timestamp");
            }
        }
        return time;
    }

    private String timestampToString(final long timestamp) {
        final Instant instant = Instant.ofEpochMilli(timestamp);
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS'Z'")
                .withZone(ZoneId.of("Z"));
        return formatter.format(instant);
    }

}
