# Nile Kaitai

**Nile Kaitai** is a Java library tailored for Kafka streaming applications that process binary data formats.
Integrating [Kaitai Struct](https://kaitai.io/), Nile Kaitai
extends [Nile Common's](https://gitlab.cern.ch/nile/streams/nile-common/-/packages) `AbstractStream`, offering
tools for parsing binary data, decoding them into structured format and wrting the output to Kafka topics.

## Getting Started

### Prerequisites

- Java 21 or higher
- [Nile Common](https://gitlab.cern.ch/nile/streams/libs/nile-common) library dependency
- [Nile Test Utils](https://gitlab.cern.ch/nile/streams/libs/nile-test-utils) library dependency
- [Kaitai Struct Runtime](https://github.com/kaitai-io/kaitai_struct_java_runtime) library dependency

### Adding Dependency

Include Nile Kaitai in your project by adding this dependency to your `pom.xml` file:

```xml
<dependency>
    <groupId>ch.cern.nile</groupId>
    <artifactId>nile-kaitai</artifactId>
    <version>1.0.0</version>
</dependency>
```

Ensure that your Maven project is configured to access the GitLab Maven repository where `nile-kaitai` is hosted:

```xml
<repositories>
    <repository>
        <id>gitlab-maven-nile-kaitai</id>
        <url>https://gitlab.cern.ch/api/v4/projects/171127/packages/maven</url>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
    </repository>
</repositories>
```

## Basic Usage

### Project Structure

The suggested project structure for a Kafka streaming application using Nile Kaitai is as follows:

```plaintext
project-root
├── src
│   ├── main
│   │   ├── java
│   │   │   └── ch.cern.nile.section_name.sensor_name
│   │   │       ├── SensorNamePacket.java (the generated Kaitai Struct class)
│   │   │       ├── SensorNameStream.java (the custom stream class)
│   │   ├── resources
│   │   │   └── kaitai
│   │   │       └── sensor_name_packet.ksy (the Kaitai Struct definition file)
│   ├── │   └── log4j.properties (optional, for logging configuration)
│   ├── test
│   │   ├── java
│   │   │   └── ch.cern.nile.section_name.sensor_name
│   │   │       ├── SensorNamePacketTest.java (the test class for the Kaitai Struct)
│   │   │       ├── SensorNameStreamTest.java (the test class for the custom stream)
│   │   ├── resources
│   │   │   └── data
│   │   │       └── frame_name.json (sample data in JSON format)
├── pom.xml
├── CHANGELOG.md (Keep a changelog of the project)
├── README.md (Project documentation)
├── .gitignore
├── .gitlab-ci.yml
```

### 1. Write a Kaitai Struct Definition

Create a `.ksy` file describing your binary data structure. Refer to the [Kaitai Struct website](https://kaitai.io/) for
examples and documentation. Alternatively, most of CERN
Nile's [streaming applications](https://gitlab.cern.ch/nile/streams/apps) include a `.ksy` file in
their `resources/kaitai` directory.

#### Custom Reserved Keywords in Structs

Nile Kaitai supports custom reserved keywords in Kaitai Struct definitions to provide additional control over the
parsing:

- **hidden**: This keyword is used to annotate fields within the `.ksy` definitions that should not be included in the
  output data structure after parsing. It is used either to hide fields that are not relevant to the application or
  fields that are used to compute other fields and are not meant to be directly accessed.

   ```yaml 
   meta:
      id: lora_hum_temp_batmon_packet
      endian: le
      bit-endian: le
   
   seq:
      - id: packet_number_low_hidden
        type: u1
      - id: packet_number_high_hidden
        type: u1
      - id: swbuild
        type: u1
      # ... other fields ...
   ```

- **payload**: This keyword indicates fields that contain data for multiple different frames, and allows switching
  between them depending on the frame type.

   ```yaml
   meta:
      id: access_control_packet
      endian: be
      bit-endian: be
   
   seq:
      - id: frame_code
        type: u1
      - id: payload
        type:
           switch-on: frame_code
           cases:
              64: data_frame
              48: keep_alive_frame
              _: unsupported_frame
   types:
      data_frame:
         seq:
            - id: status_byte
              type: status_byte_bits
            - id: channel_1_info
              type: u2
            # ... other fields ...
     # ... other frames ...
   ```

### 2. Compile the Kaitai Struct

Use the Kaitai Struct compiler to generate the Java class from the `.ksy` file. The generated class will be used to
parse the binary data in the streaming application.

   ```bash
   kaitai-struct-compiler --java <path-to-ksy-file> -t java --outdir <output-directory> 
   ```

Make sure to add your package name to the generated class file (e.g. `package ch.cern.nile.asg.cracksensors;`).

### 3. Implement a Custom Streaming Application

Create a class extending `LoraDecoderStream`, providing the Kaitai Struct class for decoding.

```java
import java.util.Collections;
import ch.cern.nile.kaitai.streams.LoraDecoderStream;

public class CrackSensorsStream extends LoraDecoderStream<CrackSensorsPacket> {
    public CrackSensorsStream() {
        super(CrackSensorsPacket.class);
    }
}
```

### 4. Write Tests for the Kaitai Struct

Create a test class for the Kaitai Struct to ensure that the binary data is correctly decoded.

```java
import ch.cern.nile.kaitai.decoder.KaitaiPacketDecoder;
import ch.cern.nile.test.utils.TestUtils;

class CrackSensorsPacketTest {
    private static final JsonElement LORA_CRACK_SENSORS_DATA_FRAME = TestUtils.getDataAsJsonElement("AAc/3sLha7FAX8b9");

    @Test
    void givenValidData_whenDecoding_thenCorrectlyDecoded() {
        final Map<String, Object> packet = KaitaiPacketDecoder.decode(CRACK_SENSORS_DATA, CrackSensorsPacket.class);
        assertEquals(18.55, packet.get("temperature"), "Incorrect temperature");
        assertEquals(3545.62, packet.get("batteryVoltage"), "Incorrect battery voltage");
        assertEquals(-112.710_335f, packet.get("displacementRaw"), "Incorrect raw displacement");
        assertEquals(3.496_520_3f, packet.get("displacementPhysical"), "Incorrect physical displacement");
    }
}
```

### 5. Write Tests for the Custom StreamING Application

Use the `StreamTestBase` class from Nile Test Utils to test the custom stream application.

```java
import ch.cern.nile.kaitai.streams.TestStream;
import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;

class StreamTest extends StreamTestBase {

    private static final JsonObject RECORD_CORRECT = TestUtils.loadRecordAsJson("data/record_correct.json");

    @Override
    public TestStream createStreamInstance() {
        return new TestStream();
    }

    @Test
    void givenCorrectData_whenDecoding_thenOutputRecordIsCreatedWithExpectedValues() {
        waitForEmptyOutputTopicAndPipeRecord(RECORD_CORRECT);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        final JsonObject outputValue = outputRecord.value();

        assertNotNull(outputValue, "Output value should not be null");

        assertFalse(outputValue.has(""), "Expected empty field replacing temperature to be ignored");
        assertFalse(outputValue.has("temperature"), "Expected temperature to be ignored");
        assertTrue(outputValue.has("FRAMETYPE"), "Expected frame type to be added");
        assertEquals(3545.62, outputValue.get("batteryVoltage").getAsDouble(), "Incorrect battery voltage");
        assertEquals(-112.710_335f, outputValue.get("displacementRaw").getAsFloat(), "Incorrect raw displacement");
        assertEquals(3.496_520_3f, outputValue.get("displacementPhysical").getAsFloat(),
                "Incorrect physical displacement");
        // ... other assertions ...
    }
}
```

## Advanced Usage

### Adding custom fields to the output records, renaming fields, & removing fields

For use cases where custom fields are relevant and exist in the source data, they can be added to the output records by
defining a map of field names and using the `LoraDecoderStream` constructor that accepts a map of custom fields.

Furthermore, the map can be used to rename fields and remove fields from the output records.

```java
class TestStream extends LoraDecoderStream<CrackSensorsPacket> {
    private static final Map<String, String> CUSTOM_METADATA_FIELD_MAP = new HashMap<>();

    static {
        CUSTOM_METADATA_FIELD_MAP.put("adr", "adr");    // adding a custom field available in the source data
        CUSTOM_METADATA_FIELD_MAP.put("temperature", ""); // removing a field from the output record
        CUSTOM_METADATA_FIELD_MAP.put("batteryVoltage", "battery_voltage"); // renaming a field
    }

    TestStream() {
        super(CrackSensorsPacket.class, CUSTOM_METADATA_FIELD_MAP, true, true, true);
    }
}
```

### Adding Gateway Details to Output Records

For use cases where the gateway details are relevant, it is useful to add the gateway details to the output records.
This can be done by setting the `addGatewayDetails` parameter to `true` in the `LoraDecoderStream` constructor.

```java
public class CrackSensorsStream extends LoraDecoderStream<CrackSensorsPacket> {
    public CrackSensorsStream() {
        super(CrackSensorsPacket.class, Collections.emptyMap(), false, true, false);
    }
}
```

### Adding Transmission Details to Output Records

For use cases where the transmission details are relevant, it is useful to add the transmission details to the output
records.
This can be done by setting the `adTxDetails` parameter to `true` in the `LoraDecoderStream` constructor.

```java
public class CrackSensorsStream extends LoraDecoderStream<CrackSensorsPacket> {
    public CrackSensorsStream() {
        super(CrackSensorsPacket.class, Collections.emptyMap(), false, false, true);
    }
}
```

### Using custom name mappings for whole structures

For use cases that require full control over the output record field names, a custom name mapping can be provided. This
is useful for older applications that used different field names, than the Java generated Kaitai Struct class fields
names

A custom name mapping must be placed in the `resources` directory of the project, in a file
named `<generated-kaitai-java-class>.json`. Any field that is not present in the custom name mapping will keep its
default name.

```json
{
  "frameType": "FRAMETYPE"
}
```

### Custom enrichment

For use cases that require custom enrichment of the output records, a custom enrichment function can be provided in the
Stream class. This is useful for performing custom operations on the output records, such as filtering out records,
adding custom fields, or modifying existing fields. It is suggested to avoid using the custom enrichment function for
use cases that can be handled by the Kaitai Struct definition.

```java
public class ForkliftLocationStream extends LoraDecoderStream<ForkliftLocationPacket> {

    public ForkliftLocationStream() {
        super(ForkliftLocationPacket.class, true, false, false);
    }

    @Override
    public Map<String, Object> enrichCustomFunction(Map<String, Object> inputMap, JsonObject metadataJson) {
        if (((double) inputMap.get("latitude") == 0.0 && (double) inputMap.get("longitude") == 0.0) ||
                !((boolean) inputMap.get("gnssFix"))) {
            return null;
        }
        return inputMap;
    }

}
```

## Documentation

Javadoc documentation for Nile Kaitai can be found [here](https://nile-kaitai.docs.cern.ch).

## Support & Contact

For support, questions, or feedback, please contact [Nile Support](mailto:nile-support@cern.ch).